<section id="about" class="container content-section text-center">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <h2>About CSD</h2>
                <p>All Kerala IEEE Computer Society Day (CSD) is a novel initiative organised by the IEEE Student Branch, CEC. 
Although there is an All India Computer Society Student Congress, there is no counterpart of such an 
event at the section level. Hence we see such an event as an imperative to facilitate interaction between 
the CS members across Kerala. <br>
CSD's focal point shall be CS membership development and retention. 
It envisages the All Kerala Computer Society Student Congress in 2015 as its successor
</p>
        </div>
    </div>
    </section>