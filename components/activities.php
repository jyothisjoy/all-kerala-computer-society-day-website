<section id="activities"  class="container text-center">
     <h2>CS ACTIVITIES IN CEC SB</h2>
     <div class="row">
    <div class="col-lg-4">
      <div class="panel-group" id="accordion">
         
         <div class="panel panel-success">
    
          
          <h4>
            <a data-toggle="collapse" data-parent="#accordion" href="#accordionone">
            <i class="fa fa-users fa-4x" style="color:#000;"></i>
            <br>
              GUILD
            </a>
          </h4>
          
     
        <div id="accordionone" class="panel-collapse collapse">
          <div class="panel-body">
            <p>CS members of semester 3 are divided into "guilds" to compete among themselves for the coveted "Best Guild Award" from the SB. Each team has a senior CS member as team lead. Guild activities include weekly tech talks, competitions and seminars organized and conducted by each team. </p>
          </div>
        </div>
    
      </div>
     </div>
   </div>

<div class="col-lg-4">
      <div class="panel-group" id="accordion">
         
         <div class="panel panel-success">
    
          
          <h4>
            <a data-toggle="collapse" data-parent="#accordion" href="#accordionTwo">
            <i  class="fa fa-bar-chart-o fa-4x" style="color:#000;"></i>
            <br>
              ISQIP
            </a>
          </h4>
          
     
        <div id="accordionTwo" class="panel-collapse collapse">
          <div class="panel-body">
          <p>  IEEE Student Quality Improvement Program is a week long intensive internship training program offered to 3 and 5 semester students, aimed at creating industry-ready professionals at the undergraduate level. The 2014 edition of ISQIP had 120 students in its cohort.</p>
          </div>
        </div>
    
      </div>
     </div>
   </div>

<div class="col-lg-4">
      <div class="panel-group" id="accordion">
         
         <div class="panel panel-success">
    
          
          <h4>
            <a data-toggle="collapse" data-parent="#accordion" href="#accordionthree">
            <i class="fa fa-desktop fa-4x" style="color:#000;"></i>
            <br>
              DEV-COM
            </a>
          </h4>
          
     
        <div id="accordionthree" class="panel-collapse collapse">
          <div class="panel-body">
            <p>The Developer Community aka DevCom is a cohort based project launched in 2014. Separate entities manage Web Design, Web Development and App Development. Implemented in two stages - Phase one is underway - training and Phase Two is product development.</p>
          </div>
        </div>
    
      </div>
     </div>
   </div>

     </div>
    </section>