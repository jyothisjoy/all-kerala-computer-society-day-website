<section id="committee" class="container-fluid text-center">
<br>
<br>   
    <h2>COMMITTEE</h2>
<br>    
    <div class="row">
       <div class="col-lg-2 col-lg-offset-1">
         <img src="img/com/thasmi.jpg" alt="Picture" height="200px;" class="img-circle"><br>
         <span id="namecom">Thasmi K Maheen</span><br>
         <span id="descom">Convener</span>
       </div>
       <div class="col-lg-2">
         <img src="img/com/paul.jpg" alt="Picture" height="200px;" class="img-circle"><br>
         <span id="namecom">Paul K George</span><br>
         <span id="descom">Joint-Convener</span>
       </div>
       <div class="col-lg-2">
         <img src="img/com/aravind.jpg" alt="Picture" height="200px;"  class="img-circle"><br>
         <span id="namecom">Aravind Sunil</span><br>
         <span id="descom">Administrator</span>
       </div>
       <div class="col-lg-2">
         <img src="img/com/leroy.jpg" alt="Picture" height="200px;" class="img-circle"><br>
         <span id="namecom">Leroy J Vargis</span><br>
         <span id="descom">Online Publicity</span>
       </div>
       <div class="col-lg-2">
         <img src="img/com/susmi.jpg" alt="Picture" height="200px;" class="img-circle"><br>
         <span id="namecom">Susmita M</span><br>
         <span id="descom">Offline Publicity</span>
       </div>

    </div>
<br>
<br>
    <div class="row">
      <div class="col-lg-2 col-lg-offset-1">
         <img src="img/com/alina.jpg" alt="Picture" height="200px;"  class="img-circle"><br>
         <span id="namecom">Alina Benny</span><br>
         <span id="descom">Documentation</span>
       </div>
       <div class="col-lg-2">
         <img src="img/com/joe.jpg" alt="Picture" height="200px;"  class="img-circle"><br>
         <span id="namecom">Jyothis Joy</span><br>
         <span id="descom">Website</span>
       </div>
       <div class="col-lg-2">
         <img src="img/com/bhargav.jpg" alt="Picture" height="200px;"  class="img-circle"><br>
         <span id="namecom">Bhargav M</span><br>
         <span id="descom">Venue Management</span>
       </div>
       <div class="col-lg-2">
         <img src="img/com/ans.jpg" alt="Picture" height="200px;"  class="img-circle"><br>
         <span id="namecom">Ansu Mathew</span><br>
         <span id="descom">Event Management</span>
       </div>
       <div class="col-lg-2">
         <img src="img/com/kathu.jpg" alt="Picture"  height="200px;" class="img-circle"><br>
         <span id="namecom">Karthika S</span><br>
         <span id="descom">Event Management</span>
       </div>
    </div>
<br>
<br> 
    <div class="row">
      <div class="col-lg-2 col-lg-offset-1">
         <img src="img/com/rony.jpg" alt="Picture" height="200px;"  class="img-circle"><br>
         <span id="namecom">Rony Alex</span><br>
         <span id="descom">Event Management</span>
       </div>
       <div class="col-lg-2">
         <img src="img/com/nived.jpg" alt="Picture" height="200px;"  class="img-circle"><br>
         <span id="namecom">Nived Krishnan</span><br>
         <span id="descom">Registration</span>
       </div>
       <div class="col-lg-2">
         <img src="img/com/ansu.jpg" alt="Picture" height="200px;"  class="img-circle"><br>
         <span id="namecom">Ansu Anish</span><br>
         <span id="descom">Registration</span>
       </div>
       <div class="col-lg-2">
         <img src="img/com/ashish.jpg" alt="Picture" height="200px;"  class="img-circle"><br>
         <span id="namecom">Ashish Muralidharan</span><br>
         <span id="descom">Registration</span>
       </div>
       
      </div>
   
<br>
<br>      
    </section>