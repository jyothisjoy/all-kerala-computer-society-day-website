<section id="contact" class="container content-section text-center">
        <div class="row">
            <div class="col-lg-4 text-center">
              <img src="img/ear.png">
            </div>
            <div class="col-lg-4 text-left">
                <h2><i class="fa fa-mobile"></i> Contact</h2>
                     <p>
                       <a href="http://www.facebook.com/akcsday" target="_blank">Facebook</a><br>
                       E-mail: <a href="mailto:csd@cecieee.org" target="_blank">csd@cecieee.org</a> </p>
                    <p>   Chairman,<br>
                       IEEE Student Branch,<br>
                       College Of Engineering, Chengannur</p>
                     <p>Joint - Convener: <br>Paul K George : +91 9961-558-262<br><br>
                      Administrator : <br>Aravind Sunil : +91 8547-292-579</p>
            </div>
            <div class="col-lg-4 text-left" style="padding-right:60px;">
                <h2><i class="fa fa-car"></i> How to Reach</h2>
                CEC is located in Chengannur, a beautiful town in Kerala. Main Central Road (popularly known as the M.C. Road) passes by the college. It is well connected to other cities by rail and road. 
                 <br>
                 <br>
                <h5>By Train</h5>
                <br>
                  Take a train to Chengannur Railway Station (Code - CNGR)
                  <br>
                  Take an auto or walk to CEC (1 Km.)
                 <br>
                 <br>
                <h5>By Bus</h5>
                  Take a bus to Chengannur KSRTC Bus stand
                  <br>
                  Walk to CEC ( 1/2 km. )           
            </div>
        </div>
    </section>