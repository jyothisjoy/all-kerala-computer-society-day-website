<section class="intro">
        <div class="intro-body">
            <div class="container">
              <div class="row">
                <div class="col-lg-3">
                  <a href="http://computer.org"><img src="img/cs.png" height="60px;"></a>
                </div>
                <div class="col-lg-3 col-lg-offset-6">
                  <a href="http://ieee.org"><img src="img/ieeelogo.png" height="60px;"></a> 
                </div>
              </div>  
  
              <div class="row">
                  <div class="col-md-8 col-md-offset-2">
                        <img src="img/logo.png" height="200px;">
                        <p class="intro-text">All Kerala IEEE Computer Society Day.<br>2nd August 2014</p>
                        <div class="page-scroll">
                            <a href="#about" class="btn btn-circle">
                                <i class="fa fa-angle-double-down animated"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>