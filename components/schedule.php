<section id="schedule" class="container content-section text-center">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <h2>Schedule</h2>
                
                <div class="table-responsive">
                    <table class="table">
                      <tr class="success">
                          <td>Time</td>
                          <td class="text-left">Speakers</td>
                          <td class="text-left" id="sch">Event</td>
                      </tr>
                      <tr class="warning">
                          <td>7.30</td>
                          <td class="text-left"></td>
                          <td class="text-left" id="sch">Registration</td>
                      </tr>
                      <tr class="active">
                          <td>7.30</td>
                          <td class="text-left"></td>
                          <td class="text-left" id="sch">Ice Breaking Session</td>
                      </tr>
                      <tr class="warning">
                          <td>9.10</td>
                          <td class="text-left">Farish C V</td>
                          <td class="text-left" id="sch">
                          <a data-toggle="collapse" data-target="#accone" class="toogle">All Agog About CS</a>
                          <div id="accone" class="collapse">
                             A session on why IEEE CS is a great society to be part of.
                          </div>
                          </td>
                      </tr>
                      <tr class="active">
                          <td>9.40</td>
                          <td class="text-left">Harishankar P S</td>
                          <td class="text-left" id="sch">
                          <a data-toggle="collapse" data-target="#acctwo" class="toogle">Anecdote</a>
                          <div id="acctwo" class="collapse">
                             A short journey through the life of an IEEE aficionado – Harisankar P S
                          </div>
                          </td>
                      </tr>
                      <tr class="warning">
                          <td>10.00</td>
                          <td class="text-left">Sreenivasan, Raveendran</td>
                          <td class="text-left" id="sch">Inaugural Ceremony</td>
                      </tr>
                      <tr class="active">
                          <td>10.40</td>
                          <td class="text-left">Thasmi K Maheen</td>
                          <td class="text-left" id="sch">
                          A prelude to All Kerala IEEE Computer Society Student Congress (AKICSSC)
                          </td>
                      </tr>
                      <tr class="warning">
                          <td>11.00</td>
                          <td class="text-left">Dr. Satish Babu</td>
                          <td class="text-left" id="sch">Technical Session</td>
                      </tr>
                      <tr class="active">
                          <td>11.45</td>
                          <td class="text-left">Thasmi K Maheen</td>
                          <td class="text-left" id="sch">
                          <a data-toggle="collapse" data-target="#accthree" class="toogle">Chapter Activity Exhibition</a>
                          <div id="accthree" class="collapse">
                             A quick look at the SB’s CS activities.
                          </div>
                          </td>
                      </tr>
                      <tr class="success">
                        <td>12.30</td>
                        <td class="text-left"></td>
                        <td class="text-left">Lunch Break</td>
                      </tr>
                      <tr class="warning">
                          <td>13.30</td>
                          <td class="text-left">Irene Susan Mathew</td>
                          <td class="text-left" id="sch">
                          <a data-toggle="collapse" data-target="#accfour" class="toogle">Pearls of Wisdom</a>
                          <div id="accfour" class="collapse">
                             Up close and personal with a few REM scholars.
                          </div>
                          </td>
                      </tr>
                      <tr class="active">
                          <td>14.20</td>
                          <td class="text-left">Mini Ulanat</td>
                          <td class="text-left" id="sch">
                          <a data-toggle="collapse" data-target="#accfive" class="toogle">Gratis Expertise</a>
                          <div id="accfive" class="collapse">
                             A comprehensive analysis of the section’s CS activities and an open forum for general clarifications.
                          </div>
                          </td>
                      </tr>
                      <tr class="warning">
                          <td>14.45</td>
                          <td class="text-left">Sandeep C</td>
                          <td class="text-left" id="sch">Technical Group Activity</td>
                      </tr>
                      <tr class="active">
                          <td>15.45</td>
                          <td class="text-left">Tony Tom</td>
                          <td class="text-left" id="sch">Conclusion and feedbacks</td>
                      </tr>
                    </table>
                </div>



            </div>
        </div>
    </section>